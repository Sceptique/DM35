json.array!(@classes) do |class|
  json.extract! class, :id
  json.url class_url(class, format: :json)
end
