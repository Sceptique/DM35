class CreateClasses < ActiveRecord::Migration
  def change
    create_table :classes do |t|

      t.string  :nom, uniq: true, null: false
      t.text    :description

      # alignements: exemple : [ [Loyal, Bon], [Loyal, Neutre], [Loyal, Mauvais] ]
      t.json    :alignements

      # dXX
      t.integer :dv

      # importance des carac, -1, 0, 1, 2: -1 = osef, 0 = normal, 1 = important, 2 = primordial
      t.integer :for
      t.integer :dex
      t.integer :con
      t.integer :int
      t.integer :sag
      t.integer :cha

      # 0 mage, 1 special, 2 combat
      # 0: 0 1 1 2 2 3 3 4 4 ...
      # 1: 0 1 2 3 3 4 5 6 6 ...
      # 2: 1 2 3 4 5 6 7 8 9 ...
      t.integer :bba

      # 0 ou 1
      # 0: 0 0 1 1 1 2 2 2 3 3 3 ...
      # 1: 2 3 3 4 4 5 5 6 6 7 7 ...
      t.bool    :vig
      t.bool    :ref
      t.bool    :vol

      # competences_list: [Str, Str, ...]
      t.json    :competences_list
      # competences_points: N dans (N + competences_carac) (*4 niveau 1)
      t.integer :competences_points
      # competences_carac: "Int" ou autre
      t.string  :competences_carac

      # dons: {nom => {bonus => {competence}, type => "permanent/action X/...", description => Str, effet => Str }
      # special: { niveau => [ don, ... ] }
      # note: si un don bonus, {"don" => {type => "*/don metamagie/don martial/..." }
      t.json    :special

      # sorts_caracteristique: Int/Sag/Cha
      t.string  :sorts_caracteristique
      # sorts_par_jour: [ [n1, n1, n1, ...], [n2, n2, n2, ...], ... ]
      t.json    :sorts_par_jour
      # sorts_connus: "*" / [ [n, n, ...], [n, n, ...] ]
      t.json    :sorts_connus

      t.timestamps null: false
    end
  end
end
